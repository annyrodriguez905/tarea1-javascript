const bd =[
    {"Id":0,"Apellido":"RODRIGUEZ", "Nombre":"ANNY", "Semestre": "Quinto", 
    "Paralelo":"B", "Direccion":"ESMERALDAS", "Telefono": "0939103256",
    "Correo":"annyrodriguez905@gmail.com"},
    {"Id":1,"Apellido":"MENDEZ", "Nombre":"ELIANA", "Semestre": "Quinto", 
    "Paralelo":"B", "Direccion":"Manta", "Telefono": "0965848464",
    "Correo":"elimendez@gmail.com"},
    {"Id":2,"Apellido":"MARTINEZ", "Nombre":"DALERK", "Semestre": "Quinto", 
    "Paralelo":"A", "Direccion":"MANTA", "Telefono": "0952375769",
    "Correo":"emarDar@gmail.com"},
    {"Id":3,"Apellido":"CUAJIVOY", "Nombre":"VICTOR", "Semestre": "Quinto", 
    "Paralelo":"A", "Direccion":"MANTA", "Telefono": "0958674839",
    "Correo":"victorcua@gmail.com"},
    {"Id":4,"Apellido":"BARRAGAN", "Nombre":"DIANA", "Semestre": "Quinto", 
    "Paralelo":"B", "Direccion":"MANTA", "Telefono": "0907985647",
    "Correo":"barragandi@gmail.com"},
    {"Id":5,"Apellido":"LOPEZ", "Nombre":"LARISSA", "Semestre": "Quinto", 
    "Paralelo":"C", "Direccion":"MANTA", "Telefono": "0965748697",
    "Correo":"lariss@gmail.com"},
    {"Id":6,"Apellido":"VERA", "Nombre":"PEDRO", "Semestre": "Quinto", 
    "Paralelo":"B", "Direccion":"MANTA", "Telefono": "0985626321",
    "Correo":"pedritov@gmail.com"},
    {"Id":7,"Apellido":"MEZA", "Nombre":"WINTER", "Semestre": "Quinto", 
    "Paralelo":"A", "Direccion":"MANTA", "Telefono": "0912345978",
    "Correo":"winterm@gmail.com"},
    {"Id":8,"Apellido":"VILLAMAR", "Nombre":"MARIA", "Semestre": "Quinto", 
    "Paralelo":"A", "Direccion":"MANTA", "Telefono": "0984463546",
    "Correo":"mvillamar@gmail.com"},
    {"Id":9,"Apellido":"SANCHEZ", "Nombre":"ANTHONY", "Semestre": "Quintero", 
    "Paralelo":"A", "Direccion":"MANTA", "Telefono": "0956384765",
    "Correo":"anthony@gmail.com"}
]

const estudiantes = document.querySelectorAll('.nombre_estudiante');

estudiantes.forEach((estudiante)=>{
    estudiante.addEventListener('click', (nombre)=>{
        let id=nombre.target.getAttribute('estu-id');
        bd.forEach((estudiante)=>{
            if(id == estudiante.Id){
                const verDetalle=nombre.target.parentElement.lastElementChild;
                verDetalle.innerHTML=`
                                    <div class="lista">

                                        <div class="nom">
                                        <h2>Nombre:</h2>
                                        <p>${estudiante.Nombre}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Apellido:</h2>
                                            <p>${estudiante.Apellido}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Correo:</h2>
                                            <p>${estudiante.Correo}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Telefono:</h2>
                                            <p>${estudiante.Telefono}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Direccion:</h2>
                                            <p>${estudiante.Direccion}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Semestre:</h2>
                                            <p>${estudiante.Semestre}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Paralelo:</h2>
                                            <p>${estudiante.Paralelo}</p>
                                        </div> 
                                    </div>`

            }
        })
    })
})

